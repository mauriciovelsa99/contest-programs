//P.67
bool ready[N];
int value[N];

//solve for each case and store the auxiliary results
int solve(int x) {
  if (x < 0) return INF;
  if (x == 0) return 0;
  if (ready[x]) return value[x];
  int best = INF;
  for (auto c : coins) {
    best = min(best, solve(x-c)+1);
  }
  value[x] = best;
  ready[x] = true;
  return best;
}

//if you want to calculate from the beginning of program until value n
value[0] = 0;
n = 100
for (int x = 1; x <= n; x++) {
  value[x] = INF;
  for (auto c : coins) {
    if (x-c >= 0) {
    value[x] = min(value[x], value[x-c]+1);
    }
  }
}
//if we want to store the coins used
int first[N];
value[0] = 0;
for (int x = 1; x <= n; x++) {
  value[x] = INF;
  for (auto c : coins) {
    if (x-c >= 0 && value[x-c]+1 < value[x]) {
      value[x] = value[x-c]+1;
      first[x] = c;
    }
  }
}

while (n > 0) {
cout << first[n] << "\n";
n -= first[n];
}
//and if we need co count the number of solutions...
int count[n] = {0}
count[0] = 1;
for (int x = 1; x <= n; x++) {
  for (auto c : coins) {
    if (x-c >= 0) {
      count[x] += count[x-c];
      //count[x] %= m; if i need module m
    }
  }
}
