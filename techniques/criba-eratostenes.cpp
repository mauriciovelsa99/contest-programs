#include<bits/stdc++.h>

using namespace std;

void criba(int limit,vector<bool> &isPrime, vector<int> &primes) {
  isPrime[0] = isPrime[1] = false;
  for (int i=2; i<limit; ++i) {
    if (isPrime[i]) {
      primes.push_back(i);
      for (int h=2; h*i<limit; ++h) isPrime[i*h] = 0;
    }
  }
}
int main(){
  int limit = 10; //limit of calculation
  vector<bool> isPrime(limit, true);
  vector<int> primes;
  criba(limit, isPrime, primes);
  cout << isPrime[2]; //in this case i printed if numer two is prime
  return 0;
}
