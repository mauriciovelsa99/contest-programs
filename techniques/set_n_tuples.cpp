#include <bits/stdc++.h>
using namespace std;

int main(){
  set<tuple<int,int,int>> s;
  s.insert( make_tuple(1, 2,3) );
  auto it = s.end(); it--; //greatest tuple of set (in this case i just have one)
  cout << get<1>(*it) << " " << get<2>(*it) << "\n"; // 2 3
  return 0;
}
