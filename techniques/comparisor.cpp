#include <iostream>
#include <set>
#include <utility>

struct comp
{
	template<typename T>
	bool operator()(const T& l, const T& r) const
	{
		if (l.first == r.first)
			return l.second > r.second;

		return l.first < r.first;
	}
};

int main()
{
	std::set<std::pair<std::string,int>, comp> set = {
		{"A", 4}, {"B", 4}, {"C", 1}, {"A", 0}, {"B", 3}
	};

	for (auto const &p: set) {
		std::cout << "{" << p.first << ":" << p.second << "}\n";
	}

	return 0;
}
