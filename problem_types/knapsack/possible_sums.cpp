/* p.72
Given a list of weights
w = [w1,w2,...,wn], determine all sums that can be constructed using the weights.*/
/*COMPLEXITY 0(nW) where n is nuber of weights */

/*this implementation is more complex than the other because the other has just one dimentional matrix*/
possible[0][0] = true;
for (int k = 1; k <= n; k++) {
  for (int x = 0; x <= W; x++) {
    if (x-w[k] >= 0) possible[x][k] |= possible[x-w[k]][k-1];
    possible[x][k] |= possible[x][k-1];
  }
}
/**/

#include<bits/stdc++.h>

using namespace std;

int main(){
  //w[num_weights] is the array cotaining weights
  //W repreents the total sum of weights w[num_weights]
  //boolean array possible of size W+1 where possible[n] means that n is possible or not
  //CAUTION BECAUSE THIS GOES FROM RIGHT TO LEFT IN SECOND LOOP, ONLY VALID IF WEIGHTS ARE JUST POSITIVE AND CERO (you can remove cero from weights, actually)
  int num_weights = 4;
  int w[num_weights] = {1,3,3,5};
  int W = accumulate(w, w + sizeof(w)/sizeof(w[0]), 0);
  bool possible[W+1] = {false};

  possible[0] = true;
  for (int k = 0; k < num_weights; k++) {
    for (int x = W; x >= 0; x--) {
      if (possible[x]) possible[x+w[k]] = true;
    }
  }
  //print result
  for(int i=0; i<=W;i++){
    cout << "number " << i << ": "<< possible[i] <<"\n";
  }
  return 0;
}
