P.72
The term knapsack refers to problems where a set of objects is given, and
subsets with some properties have to be found. Knapsack problems can often be
solved using dynamic programming.
