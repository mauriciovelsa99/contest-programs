#include<bits/stdc++.h>

using namespace std;

int main(){
  int N, count = 0;
  int factorial[9];
  factorial[1] = 1;
  for(int i=2;i<9; i++) {
    factorial[i] = factorial[i-1]*i;
  }
  cin >> N;
  for(int i=8; i>0; i--){
    while(N - factorial[i] >= 0) {
      N -= factorial[i];
      count++;
    }
    if(N == 0) break;
  }
  cout << count << "\n";
  return 0;
}
