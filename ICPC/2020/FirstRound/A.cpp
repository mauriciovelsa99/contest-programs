#include<bits/stdc++.h>

using namespace std;

int main(){
  //freopen("input.txt", "r", stdin);
  int N, Q;
  int i,j, repetitions;
  char letter;
  cin >> N >> Q;
  set< pair<int, int> > sets[N];
  set<tuple<int,int,int>> max_sets;
  int items[N][N] = {0};
  for(int n = 0; n < Q; n++){
    cin >> letter;
    if(letter == 'R'){
      //if R, then sum 1 to position i,j in matrix and add new pair of values(repetitions, j) to set[i] (erase old one)
      cin >> i >> j;
      repetitions=items[i][j]+=1;
      if(repetitions != 1) sets[i].erase(make_pair(repetitions-1, j));
      sets[i].insert(make_pair(repetitions, j));
      //add to B the greatest of this set
      auto it = sets[i].end(); it--;
      max_sets.insert( make_tuple(it->first, i,it->second) );
    } else if (letter == 'Q'){
      //if Q then in case there are more than one element, just check if both greatest elements have same repetitions(it and it2).
      cin >> i;
      if (sets[i].size() > 1){
        auto it = sets[i].end(); it--;
        auto it2 = sets[i].end(); it2--; it2--;
        if(it->first == it2->first) {
          cout << "Multiple\n";
        } else {
          cout << it->second << "\n";
        }
      } else if(sets[i].size()== 1){
        auto it = sets[i].end(); it--;
        cout << it->second << "\n";
      } else {
        cout << "No info\n";
      }

    }else { // B
      //Same as Q, grab both greatest and check if they have same repetitions (remember that this set wtores ternary tuples,
      // so its a little bit different syntax grabbing elements)
      if (max_sets.size() > 1){
        auto it = max_sets.end(); it--;
        auto it2 = max_sets.end(); it2--; it2--;
        if(get<0>(*it) == get<0>(*it2)) {
          cout << "Multiple\n";
        } else {
          cout << get<1>(*it) << " " << get<2>(*it) << "\n";
        }
      } else if(max_sets.size()== 1){
        auto it = max_sets.end(); it--;
        cout << get<1>(*it) << " " << get<2>(*it) << "\n";
      } else {
        cout << "No info\n";
      }
    }
  }
  return 0;
}
