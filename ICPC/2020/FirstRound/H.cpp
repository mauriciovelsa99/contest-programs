#include<bits/stdc++.h>

using namespace std;
int main(){
  int T;
  int N;
  int candy;
  cin >> T;
  for(int t= 0; t<T; t++){
    cin >> N;
    int houses[N];
    //guardo dulces
    for(int c=0; c< N; c++){
      cin >> candy;
      houses[c] = candy;
    }
    //dinamico
    int answer[N];
    answer[0]= houses[0];
    answer[1] = houses[1];
    answer[2] = houses[0] + houses[2];
    for(int i=3;i< N;i++){
      answer[i] = max(answer[i-2] + houses[i], answer[i-3]+houses[i]);
    }
    cout << *max_element(answer, answer + N)<< "\n";
  }
  return 0;
}
