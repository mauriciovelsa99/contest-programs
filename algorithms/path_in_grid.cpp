//P.71
/*Find a path from the upper-left corner to the lower-right
corner of an n × n grid, such that we only move down and right. Each square
contains a positive integer, and the path should be constructed so that the sum of
the values along the path is as large as possible*/
/*start from (1,1) to (n,n)*/
/*fill with zeroes and then ask the input starting from 1,1, so when x,y=0, no points added because there is no way*/
int sum[N][N] = {0};

for (int y = 1; y <= n; y++) {
  for (int x = 1; x <= n; x++) {
    sum[y][x] = max(sum[y][x-1],sum[y-1][x])+value[y][x];
  }
}
