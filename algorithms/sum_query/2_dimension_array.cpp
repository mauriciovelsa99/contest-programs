//P.84
//Sum queries using prefix array called sum that can be constructed in O(n) time
//any sum can be calbulated in O(1) time with formula sum(a,b) = sum(0,b) - sum(0,a-1)
#include<bits/stdc++.h>

using namespace std;

int main(){
  int size_a = 3, size_b = 3;
  int array[size_a][size_b] = {
    {1,2,3},
    {4,5,6},
    {7,8,9}
  };
  //initialize matrix similarly of how to query from (i1,j1) to (i2,j2)
  int acc_sum[size_a][size_b];
  for(int i = 0;i<size_a;i++){
    for(int j= 0;j<size_b;j++){
      acc_sum[i][j] = array[i][j] + ((!j)? 0 : acc_sum[i][j-1]) + ((!i)? 0 : acc_sum[i-1][j]) - ((i && j)? acc_sum[i-1][j-1] : 0);
    }
  }
  //print matrix of rectangle sums
  for(int i = 0;i<size_a;i++){
    for(int j= 0;j<size_b;j++){
      cout << acc_sum[i][j] << " ";
    }
    cout << endl;
  }
  //query sum from (i1,j1) to (i2,j2)
  cout << "QUERY" << endl;
  int i1=1, j1=1, i2=2, j2=1;
  cout << acc_sum[i2][j2] - ((!j1)? 0 : acc_sum[i2][j1-1]) - ((!i1)? 0 : acc_sum[i1-1][j2]) + ((i1 && j1)? acc_sum[i1-1][j1-1] : 0) << endl;



  return 0;
}
