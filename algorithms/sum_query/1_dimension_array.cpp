//P.84
//Sum queries using prefix array called sum that can be constructed in O(n) time
//any sum can be calbulated in O(1) time with formula sum(a,b) = sum(0,b) - sum(0,a-1)
#include<bits/stdc++.h>

using namespace std;

int main(){
  int array[] = {1,2,3,4,5,6};
  int arraySize = sizeof(array)/sizeof(array[0]);
  int sum[arraySize];
  int a,b, res;
  //store accumulated sums in sum
  sum[0] = array[0];
  for(int i = 1; i<arraySize;i++){
    sum[i] = array[i] + sum[i-1];
  }
  //suma de [a,b]
  a = 1;
  b = 3;
  //if a its 0, then i dont need to substract anything
  res = sum[b] - ((!a)? 0 : sum[a-1]);
  cout << res << endl;
  return 0;
}
