/*p.49*/

/*using recursion*/
void search() {
  if (permutation.size() == n) {
  // process permutation
  } else {
    for (int i = 0; i < n; i++) {
      if (chosen[i]) continue;
      chosen[i] = true;
      permutation.push_back(i);
      search();
      chosen[i] = false;
      permutation.pop_back();
    }
  }
}

/*Another method for generating permutations is to begin with the permutation
{0,1,...,n − 1} and repeatedly use a function that constructs the next permutation in increasing order. The C++ standard library contains the function
next_permutation that can be used for this*/
/*IMPORTANT: VECTOR NEED TO BE IN INCREASING ORDER TO PROCESS ALL PERMUTATIONS*/
/*AFTER ENDING LOOP, VECTOR WILL END IN INCREASING ORDER (if you start in increasing, then it will end at same state)*/
vector<int> permutation;
for (int i = 0; i < n; i++) {
permutation.push_back(i);
}
do {
// process permutation
} while (next_permutation(permutation.begin(),permutation.end()));
