//both with complexity O(log n)
//binary search with a ascendent ordered array
int a = 0, b = n-1;
while (a <= b) {
int k = (a+b)/2;
if (array[k] == x) {
// x found at index k
}
if (array[k] > x) b = k-1;
else a = k+1;
}

//the other method is by "jumps"
/*The search goes through the array from left to right, and the initial jump
length is n/2. At each step, the jump length will be halved: first n/4, then n/8,
n/16, etc., until finally the length is 1. After the jumps, either the target element
has been found or we know that it does not appear in the array. */
int k = 0;
for (int b = n/2; b >= 1; b /= 2) {
while (k+b < n && array[k+b] <= x) k += b;
}
if (array[k] == x) {
// x found at index k
}
