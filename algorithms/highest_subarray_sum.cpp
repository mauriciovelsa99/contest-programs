//p 23 of book Kadane´s Algorithm
int best = 0, sum = 0;
for (int k = 0; k < n; k++) {
  sum = max(array[k],sum+array[k]);
  best = max(best,sum);
}
cout << best << "\n";

/* Algoritmo para encontrar subarray continuo con la mayor suma, complejidad O(n)
Sean 3 partes a,b,c de un arreglo donde a y c tienen positivos y b tiene negativos
si recorremos a nos ira subiendo la suma y el algoritmo aumentará el best
al cruzar por b, el alogitmo irá restando puntos, el best se mantendra igual y de aquí surgirán dos casos:
  Al finalizar de recorrer c la suma > 0
  Al finalizar de recorrer c la suma <= 0
En el primer caso me conviene considerar el segmento a y b y seguir contando lo de c, ya que son puntos extra para cuando evalúe todo c y puede surgir un nuevo best con todo el segmento completo.
En el otro caso, es preferible comenzar a contar de nuevo desde c, ya que no tiene sentido restarle puntos por lo anterior (o sumar 0)

Eso es lo que hace entonces el sum = max(array[k],sum+array[k]):
  Si está en negativo o 0 la suma y evalúa un positivo, toma todo el positivo y deja lo negativo a un lado porque ya es irrelevante
  Si está en positivo y recibe un negativo, va reduciendo la suma, siempre y cuando sea > 0, me conviene seguir teniendo esa cantidad, ya que todo ese segmento juntado con otro podría ser el nuevo best

Y la línea best = max(best,sum) Sólo guarda el mejor durante toda la ejecución
*/
