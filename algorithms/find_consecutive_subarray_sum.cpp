//uses two pointers technique
//Complexity 0(n)
//IMPORTANT: JUST NUMBERS >= 0 IN THE ARRAY
#include<bits/stdc++.h>
using namespace std;

int main(){
  int A[] = {-2, 1, 1,3,-3, -1};
  int findNumber = 5;
  bool found = false;
  unsigned int arrayLength = *(&A + 1) - A;
  int lp = 0, rp = 0, sum = 0;

  for(lp = 0;lp + rp <= 2*(arrayLength-1); lp++){
    if (rp < lp) rp++;
    while(rp <= arrayLength-1 && sum + A[rp] <= findNumber) {
      sum += A[rp];
      rp++;
      if (sum == findNumber) break;
    }
    if (sum == findNumber){
      found = true;
      rp--;
      break;
    }
    if (rp > lp) sum -= A[lp];
  }
  tuple<bool, unsigned int, unsigned int> consec_subarray = make_tuple(found, lp, rp);
  if (found) {
    //xE[lp (left pointer), rp(right pointer)] (incluive)
    cout << "INDICES" << endl;
    cout << lp << endl;
    cout << rp << endl;
    cout << "NUMEROS"<< endl;
    for(int i = lp; i<=rp; i++){
      cout << A[i] << endl;
    }
  }else {
    cout << "NOT FOUND" << endl;
  }
  return 0;
}
