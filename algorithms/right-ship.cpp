/*(right shift) Takes two numbers, right shifts the bits of the first operand,
 the second operand decides the number of places to shift.Similarly right shifting (x>>y) is equivalent to dividing x with 2^y.*/
 #include<stdio.h>

using namespace std;
int main()
{
    // a = 5(00000101), b = 9(00001001)
    unsigned char a = 5, b = 9;

    // The result is 00000010

    printf("a>>1 = %d\n", a>>1);

    // The result is 00000100
    printf("b>>1 = %d\n", b>>1);
    return 0;
}
