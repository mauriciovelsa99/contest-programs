#include<bits/stdc++.h>

using namespace std;

int main(){
  int N, perDayCost;
  while(cin >> N){
    cin >> perDayCost;
    int revenue[N];
    for(int i = 0; i < N; i++){
      cin >> revenue[i];
    }
    int best = 0, sum = 0;
    for (int k = 0; k < N; k++) {
      sum = max(revenue[k]-perDayCost,sum+revenue[k]-perDayCost);
      best = max(best,sum);
    }
    cout << best << "\n";
  }
  return 0;
}
