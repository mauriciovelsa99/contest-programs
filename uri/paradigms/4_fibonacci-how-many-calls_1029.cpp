#include<bits/stdc++.h>

using namespace std;

int main(){
  int fib[40];
  fib[0] = 0;
  fib[1] = 1;
  int calls[40];
  calls[0] = 0;
  calls[1] = 0;

  for(int i = 2; i<40; i++){
    fib[i] = fib[i-1]+ fib[i-2];
    calls[i] = calls[i-1] + calls[i-2] + 2;
  }

  int N;
  int num;
  cin >> N;

  for(int i = 0; i<N; i++) {
    cin >> num;
    printf("fib(%d) = %d calls = %d\n", num, calls[num], fib[num]);
  }
  return 0;
}
