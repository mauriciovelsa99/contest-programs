#include<bits/stdc++.h>

using namespace std;

int main(){
  //freopen("input.txt", "r", stdin);
  int N, D;

  while(cin >> N >> D && !(N==0 && D == 0)){
      string inp, out;
      int removed = 0;
      cin >> inp;
      for(auto it =inp.begin(); it !=inp.end(); it++){
        //if empty, then just save it
        if (out.empty()) {
          out.push_back(*it);
        } else { //check if i can remove smaller numbers before adding it
          while(!out.empty()) {
            if ( out.back() < *it && removed < D ) {
                out.pop_back();
                removed++;
            } else {
              break;
            }
          }
          //add if is still space
          if(out.size() < N-D){
            out.push_back(*it);
          }
        }
      }
      printf("%s\n", &out[0]);
  }

  return 0;
}
