#include <bits/stdc++.h>
using namespace std;

int main(){
  map<char,int> numbOfLed;
  numbOfLed['1'] = 2;
  numbOfLed['2'] = 5;
  numbOfLed['3'] = 5;
  numbOfLed['4'] = 4;
  numbOfLed['5'] = 5;
  numbOfLed['6'] = 6;
  numbOfLed['7'] = 3;
  numbOfLed['8'] = 7;
  numbOfLed['9'] = 6;
  numbOfLed['0'] = 6;

  int testCases;
  cin >>testCases;
  string number;
  int answer;
  while(testCases>0) {
    answer = 0;
    cin >> number;
    for(int i = 0; i< number.length(); i++) {
      answer += numbOfLed[number[i]];
    }
    cout<<answer<<" leds"<<endl;
    testCases--;
  }

  return 0;
}
