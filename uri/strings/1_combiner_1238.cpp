#include <bits/stdc++.h>
using namespace std;

int main(){
  string a,b,final;
  int testCases, max_length;
  cin >> testCases;
  for(int i = 0; i< testCases; i++){
    final.erase();
    cin >> a >> b;
    max_length = a.length() > b.length()? a.length() : b.length();
    for (int k = 0; k<max_length; k++){
      if (a.length() > k ) cout << a[k];
      if (b.length() > k ) cout << b[k];
    }
    cout <<endl;
  }
  return 0;
}
