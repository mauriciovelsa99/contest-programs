#include <bits/stdc++.h>
using namespace std;

int main(){
  double pi = 3.14159;
  double number;
  cin >> number;
  double area = pi *number*number;
  cout<<setprecision(4)<<fixed;
  cout <<"A="<<area<<endl;


  return 0;
}
