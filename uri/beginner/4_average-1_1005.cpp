#include <bits/stdc++.h>
using namespace std;

int main(){
  double a, b, media;
  cin >> a >>b;
  media = (3.5*a + 7.5*b)/11;
  cout <<fixed<<setprecision(5);
  cout << "MEDIA = "<<media << endl;
  return 0;
}
