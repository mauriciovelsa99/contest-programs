#include <bits/stdc++.h>
using namespace std;

int main(){
  int emp,hours;
  double amount;
  cin >> emp >> hours >> amount;
  amount = amount*hours;
  cout <<fixed<<setprecision(2);
  cout << "NUMBER = "<<emp<<endl<<"SALARY = U$ "<<amount<<endl;
  return 0;
}
