#include <bits/stdc++.h>
using namespace std;

int main(){
  string name;
  double salary, sale, final;

  cin >>name >> salary >> sale;

  final = salary + sale*.15;
  cout << fixed << setprecision(2);
  cout <<"TOTAL = R$ "<< final<<endl;
  return 0;
}
