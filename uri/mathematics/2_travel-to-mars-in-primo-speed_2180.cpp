#include<bits/stdc++.h>

using namespace std;

int main(){
  int number, sum_of_primes = 0, count = 0, days, hours;
  cin >> number;
  if (number == 2) {
    sum_of_primes+= 2;
    count++;
  }
  if(number%2 == 0) number++;

  for(int i = number; count<10; i+=2){
    bool isPrime = true;
    for(int j=2; j<= sqrt(i); j++){
      if (i %j == 0) isPrime = false;
    }
    if (isPrime){
      sum_of_primes+= i;
      count++;
    }
  }
  printf("%d km/h\n", sum_of_primes);
  hours = 60000000 / sum_of_primes;
  printf("%d h / %d d\n", hours, hours/24);
  return 0;
}
