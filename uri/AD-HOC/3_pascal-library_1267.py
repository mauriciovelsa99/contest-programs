N, D = map(int, input().split())
while N != 0 and D != 0:
    alumni = [1]*N
    for i in range(D):
        attended =  list(map(int, input().split()))
        for idx, (alumn,attendd) in enumerate(zip(alumni,attended)):
            if attendd == 0:
                alumni[idx] = 0
    if 1 in alumni:
        print("yes")
    else:
        print("no")
    N, D = map(int, input().split())
