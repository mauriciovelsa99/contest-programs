R = int(input())

while R != 0:
    nEqual = 0
    #get arrays of integers
    Mi = list(map(int,input().split()))
    Mt = sum(Mi)
    #get sum
    Li = list(map(int,input().split()))
    Lt = sum(Li)
    #check which wins 30 points
    numM = Mi[0]
    numL = Li[0]
    countM = 1
    countL = 1
    for i in range(1, len(Mi)):
        if Mi[i] == numM:
            countM += 1
        else:
            countM = 1
            numM = Mi[i]
        if Li[i] == numL:
            countL += 1
        else:
            countL = 1
            numL = Li[i]
            
        if countM == countL and countM == 3:
            break
        elif countM == 3:
            Mt+=30
            break
        elif countL == 3:
            Lt+=30
            break
    #check who has more points
    if Mt > Lt:
        print("M")
    elif Mt < Lt:
        print("L")
    else:
        print("T")
    R = int(input())
