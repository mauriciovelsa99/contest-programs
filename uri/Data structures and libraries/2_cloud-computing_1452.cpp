#include<bits/stdc++.h>

using namespace std;

int main(){
  //freopen("input.txt", "r", stdin);
  string app;
  int N,M, server_connections;
  //make vector for each server
  while( cin >> N >> M && !(N==0 && M==0) ) {
    vector<string> server[200];
    for (int i = 0; i < N; i++){
      cin  >> server_connections;
      for(int j=0;j< server_connections;j++){
        cin >> app;
        server[i].push_back(app);
      }
      sort(server[i].begin(), server[i].end());
    }

    unordered_map<string,int> m;
    //store number of connections for each app in map
    for (int i = 0; i < N; i++){
      for (int j = 0; j < server[i].size(); j++) {
        if(m.count( server[i][j]) ){
          m[ server[i][j] ] += 1;
        }else{
          m[ server[i][j] ] = 1;
        }
      }
    }
    //count connections
    int user_connections, total = 0;
    for(int i=0; i< M ;i++){
      vector<string> apps;
      cin >> user_connections;
      for(int j= 0; j< user_connections; j++){
        cin >> app;
        apps.push_back(app);
        //count map connections
        total += m[app];
      }
      //remove duplicated connections for each server
      for (int ser = 0; ser < N; ser++){
        int count = 0;
        for (auto a : apps) { //search each app;
          if(binary_search(server[ser].begin(), server[ser].end(), a)) count++;
        }
        if(count) total = total -count +1;
      }
    }
    cout << total << "\n";
  }
  return 0;
}
