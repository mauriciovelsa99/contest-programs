while True:
    try:
        equation = input()
        opening, closing = 0, 0
        correct = True
        for c in equation:
            if c == "(":
                opening += 1
            elif c == ")":
                closing +=1

            if closing > opening:
                correct = False
                break
        print("correct" if correct and opening == closing else "incorrect")
    except EOFError: break
