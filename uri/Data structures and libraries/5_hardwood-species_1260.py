TestCases = int(input())
input()
for T in range(TestCases):
    trees = {}
    totalTrees = 0
    while True:
        try:
            tree = input()
            if tree == "": break
            totalTrees +=1
            if tree in trees:
                trees[tree]+=1
            else:
                trees[tree] = 1
        except: break
    for elem in sorted(trees):
        percentage = (float(trees[elem])/totalTrees*100)
        print(f"{elem}{percentage: .4f}")
    if T +1 < TestCases: print()
