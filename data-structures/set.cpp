/* p.37
Mantains a collection of elements

¡¡¡¡¡An important property of sets is that all their elements are distinct!!!!!
¡¡¡¡ IF ORDER DOESNT MATTER USE UNORDERED SET, ITS MORE EFICCIENT!!!


The C++ standard library contains two set implementations: The structure
set is based on a balanced binary tree and its operations work in O(logn) time.
The structure unordered_set uses hashing, and its operations work in O(1) time
on average.
The choice of which set implementation to use is often a matter of taste. The
benefit of the set structure is that it maintains the order of the elements and
provides functions that are not available in unordered_set. On the other hand,
unordered_set can be more efficient.
*/
set<int> s;
s.insert(3);
s.insert(2);
s.insert(5);
cout << s.count(3) << "\n"; // 1
cout << s.count(4) << "\n"; // 0
s.erase(3);
s.insert(4);
cout << s.count(3) << "\n"; // 0
cout << s.count(4) << "\n"; // 1
//set elements cant be accesed with [] notation, following code shows how to iterate through elements
set<int> s = {2,5,6,8};
cout << s.size() << "\n"; // 4
for (auto x : s) {
cout << x << "\n";
}
// all elements are distinct, so count is always either 0 or 1
set<int> s;
s.insert(5);
s.insert(5);
s.insert(5);
cout << s.count(5) << "\n"; // 1
/*
C++ also contains the structures multiset and unordered_multiset that other-
wise work like set and unordered_set but they can contain multiple instances of

an element. For example, in the following code all three instances of the number
5 are added to a multiset:
*/
multiset<int> s;
s.insert(5);
s.insert(5);
s.insert(5);
cout << s.count(5) << "\n"; // 3

//erase function removes all the ocurrences of element in a set
s.erase(5);
cout << s.count(5) << "\n"; // 0
//if you want to only remove one instance:
s.erase(s.find(5));
cout << s.count(5) << "\n"; // 2
