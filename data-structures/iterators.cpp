/*p.39
Many functions in the C++ standard library operate with iterators. An iterator
is a variable that points to an element in a data structure.

The often used iterators begin and end define a range that contains all ele-
ments in a data structure. The iterator begin points to the first element in the

data structure, and the iterator end points to the position after the last element.
*/

/*VECTOR ITERATORS*/
/*the following code sorts a vector using the function sort, then
reverses the order of the elements using the function reverse, and finally shuffles
the order of the elements using the function random_shuffle.*/
sort(v.begin(), v.end());
reverse(v.begin(), v.end());
random_shuffle(v.begin(), v.end());

/*These functions can also be used with an ordinary array. In this case, the
functions are given pointers to the array instead of iterators:*/
sort(a, a+n);
reverse(a, a+n);
random_shuffle(a, a+n);

/*SET ITERATORS*/
/*Iterators are often used to access elements of a set. The following code creates an
iterator it that points to the smallest element in a set:*/
set<int>::iterator it = s.begin();
/*A shorter way to write the code is as follows:*/
auto it = s.begin();
/*The element to which an iterator points can be accessed using the * symbol. For
example, the following code prints the first element in the set:*/
auto it = s.begin();
cout << *it << "\n";
/*Iterators can be moved using the operators ++ (forward) and -- (backward),
meaning that the iterator moves to the next or previous element in the set.
The following code prints all the elements in increasing order:*/
for (auto it = s.begin(); it != s.end(); it++) {
cout << *it << "\n";
}
/*The following code prints the largest element in the set:*/
auto it = s.end(); it--;
cout << *it << "\n";
/*The function find(x) returns an iterator that points to an element whose
value is x. However, if the set does not contain x, the iterator will be end.*/
auto it = s.find(x);
if (it == s.end()) {
// x is not found
}

/*The function lower_bound(x) returns an iterator to the smallest element in the
set whose value is at least x, and the function upper_bound(x) returns an iterator
to the smallest element in the set whose value is larger than x. In both functions,
if such an element does not exist, the return value is end. These functions are
not supported by the unordered_set structure which does not maintain the order
of the elements.*/

/*For example, the following code finds the element nearest to x:*/

auto it = s.lower_bound(x);
if (it == s.begin()) {
cout << *it << "\n";
} else if (it == s.end()) {
it--;
cout << *it << "\n";
} else {
int a = *it; it--;
int b = *it;
if (x-b < a-x) cout << b << "\n";
else cout << a << "\n";
}
