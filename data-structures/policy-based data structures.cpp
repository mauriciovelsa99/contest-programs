/*
The g++ compiler also supports some data structures that are not part of the c´ standard library, such are called policy-based data structures.
To use them , add following lines to code
*/
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;

/*INDEXED SET*/
/*The speciality of this set is that we have access to the indices that the elements
would have in a sorted array. */
/*Is like set but can be indexed like an array. The definition for int values is as follows:*/
typedef tree<int,null_type,less<int>,rb_tree_tag,
tree_order_statistics_node_update> indexed_set
/*now we can create it*/
indexed_set s;
s.insert(2);
s.insert(3);
s.insert(7);
s.insert(9);
/*The function find_by_order returns an iterator to
the element at a given position:*/
auto x = s.find_by_order(2);
cout << *x << "\n"; // 7
/*And the function order_of_key returns the position of a given element:*/
cout << s.order_of_key(7) << "\n"; // 2
/*If the element does not appear in the set, we get the position that the element
would have in the set:*/
cout << s.order_of_key(6) << "\n"; // 2
cout << s.order_of_key(8) << "\n"; // 3
/*Both the functions work in logarithmic time.*/
